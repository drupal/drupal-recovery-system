#!/bin/bash

for DSUID in {990000..993000}
 do
  /usr/sbin/useradd -d /dev/null -g 48 -s /sbin/nologin -M -N -u $DSUID ds$DSUID -c "drupal site $DSUID user"
 done