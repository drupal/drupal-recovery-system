#!/bin/sh

# If there are files in /etc/shibboleth/drupal/ that are not empty
# (overriden by a ScheduledJob) copy them to /etc/shibboleth
if [ -n "$(ls -A /etc/shibboleth/drupal)" ]; then
  echo "--> Copying config maps to /etc/shibboleth/ ..."
  cp /etc/shibboleth/drupal/shibboleth2.xml /etc/shibboleth/shibboleth2.xml  
  echo "DONE"
fi

exec /usr/sbin/shibd -F -c /etc/shibboleth/shibboleth2.xml