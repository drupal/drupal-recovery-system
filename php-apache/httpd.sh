#!/bin/sh

# sync sites between cephFS and local
echo "--> Copying data to local..."
drupal_version=$(ls /mnt/cephfs/ | sort | tail -n 1)
rsync -avh --stats --delete --include=sites/{all,sites.php} --exclude="*/sites/*" --delete-excluded /mnt/cephfs/${drupal_version} /mnt/data/drupal
echo "--> DONE"

# Set symlinks
echo "--> Setting symlinks..."
#ln -sf  /mnt/cephfs/7.59/sites/dev-d7-rwmanos-official.web.cern.ch /mnt/data/drupal/7.59/sites/dev-d7-rwmanos-official.web.cern.ch
for i in $(find /mnt/cephfs/${drupal_version}/sites/ -maxdepth 1 -mindepth 1 -type d -name "*.web.cern.ch" -printf '%f\n')
do
  ln -sf /mnt/cephfs/${drupal_version}/sites/${i%%/} /mnt/data/drupal/${drupal_version}/sites/${i%%/}
done
echo "--> DONE"

# If there are files in /etc/shibboleth/drupal/ that are not empty
# (overriden by a ScheduledJob) copy them to /etc/shibboleth
if [ -n "$(ls -A /etc/shibboleth/drupal)" ]; then
  echo "--> Copying config maps to /etc/shibboleth/ ..."
  cp /etc/shibboleth/drupal/shibboleth2.xml /etc/shibboleth/shibboleth2.xml  
  echo "--> DONE"
fi

# Copy all vhost config files
if [ -n "$(ls -A /tmp/conf-configmap)" ]
then
  echo "--> Copying config maps to /etc/httpd/conf/ ..."
  for f in /tmp/conf-configmap/*
  do
    if [ -s $f ]
    then
      cp /tmp/conf-configmap/* /etc/httpd/conf/
    fi
  done
  echo "--> DONE"
fi

# Copy all vhost config files
if [ -n "$(ls -A /tmp/conf-d-configmap)" ]
then
  echo "--> Copying config maps to /etc/httpd/conf.d/ ..."
  for f in /tmp/conf-d-configmap/*
  do
    if [ -s $f ]
    then
      cp /tmp/conf-d-configmap/* /etc/httpd/conf.d/
    fi
  done
  echo "--> DONE"
fi

if [[ -z $SERVICE_HOST || -z $SERVICE_PORT ]]; then
  # Transform hyphen into underscore as expected
  FORMATTED_NAME=`echo $SERVICE_NAME | tr '-' '_'`
  # Dynamically obtain the service with bash magic
  export SERVICE_HOST=$(eval echo "\${${FORMATTED_NAME^^}_SERVICE_HOST}")
  export SERVICE_PORT=$(eval echo "\${${FORMATTED_NAME^^}_SERVICE_PORT}")
fi

exec httpd -DFOREGROUND -D drupal