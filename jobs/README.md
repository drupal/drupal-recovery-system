# Jobs for recovery system on D7

It's based on 2 jobs:
- ```rsync-official-sites```: This jobs will:
    - Get the official sites info in order to prepare for copying.
    - Copy site folders as they are to cephFS disk on Openshift.
    - Copy shibboleth2.xml, needed for shibboleth.
    - Generate vhosts and settings.php per site, assigning proper user permissions and so.

- ```sync-routes-d7```: This job will:
    - Generate a route under Openshift in order the page to be accessible.
    - Restarts the php-apache pod to take into account the performed changes from previous job.

## Requirements
Generate new ssh key, upload it (ssh-copy-id) to the host and add the private key to openshift as secret:

```oc create secret generic secretsshdrupaldev --from-file=ssh-privatekey=/folder/.ssh/id_rsa```

## Deployment

```oc describe secret svcdrs-token-w0pzs```

```oc process -f sync-routes-d7.yaml SERVICEACCOUNT_TOKEN='value_taken_from_previous_command' | oc create -f -```

```oc create -f rsync-official-sites.yaml```

## Misc

Get info about jobs: ```oc get scheduledjobs```

Edit specific job: ```oc edit scheduledjob name_of_the_scheduled_job_retrieved_before```