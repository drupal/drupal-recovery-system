apiVersion: batch/v1beta1
kind: CronJob
metadata:
  labels:
    run: rsync-official-sites
  name: rsync-official-sites
  namespace: test-drupal-recovery-system
spec:
  # Execute every day at 23:00
  schedule: '0 23 * * *'
  jobTemplate:
    spec:
      activeDeadlineSeconds: 86400 # 24h: initial sync could take very long
      backoffLimit: 6
      completions: 1
      parallelism: 1
      template:
        spec:
          containers:
            - command:
              - /usr/bin/bash
              - '-c'
              - |-
                echo "Start: $(date)"

                ###########################
                # Get official sites info #
                ###########################
                sites_conf=$(ssh -o UserKnownHostsFile=/dev/null \
                                -o StrictHostKeyChecking=no \
                                -i /etc/secret-volume/ssh-privatekey \
                                root@drupaldev.cern.ch \
                                "drpadm --cat-sites-conf 2>&1 1>/dev/null | grep ';official;'")

                #####################
                # Copy site folders #
                #####################
                sites_list=($(echo "$sites_conf" | awk -F';' '{print $1}' | tr '\n' ' '));
                echo "${sites_list[@]}"
                rsync -avh \
                      --stats \
                      --delete \
                      -e "ssh -o UserKnownHostsFile=/dev/null \
                              -o StrictHostKeyChecking=no \
                              -i /etc/secret-volume/ssh-privatekey" \
                      --include=sites/{all,sites.php} \
                      ${sites_list[@]/#/--include=sites/} \
                      --exclude="*/sites/*" \
                      --delete-excluded \
                root@drupaldev.cern.ch:/mnt/data/drupal /mnt/data/

                ########################
                # Copy shibboleth2.xml #
                ########################
                rsync -tvh \
                      -e "ssh -o UserKnownHostsFile=/dev/null
                              -o StrictHostKeyChecking=no
                              -i /etc/secret-volume/ssh-privatekey" \
                      root@drupaldev.cern.ch:/etc/shibboleth/shibboleth2.xml /etc/shibboleth/drupal/

                ############################################
                # Generate vhost and settings.php per site #
                ############################################
                echo "$sites_conf" | while read site_conf; do
                  user=$(echo "$site_conf" | awk -F';' '{print $7}')
                  site=$(echo "$site_conf" | awk -F';' '{print $1}')
                  grou=$(echo "$site_conf" | awk -F';' '{print $10}')
                  if echo "$site_conf" | awk -F';' '{print $12}' | grep -q "^enforcesso$"; then
                    auth="On"
                  else
                    auth="Off"
                  fi
                  vers=$(echo "$site_conf" | awk -F';' '{print $5}')

                  #-------------------------------------------
                  # Get ServerAlias per site                 -
                  #-------------------------------------------          
                  origin_file_vhost="/mnt/data/etc/httpd/vhosts.d/${site}.conf"
                  server_aliases=$(ssh -n \
                                -o UserKnownHostsFile=/dev/null \
                                -o StrictHostKeyChecking=no \
                                -i /etc/secret-volume/ssh-privatekey \
                                root@drupaldev.cern.ch \
                                "grep ServerAlias $origin_file_vhost | awk '{print \$1, \$2}' | sort | uniq")
                  
                  file_vhost="/etc/httpd/vhosts.d/${site}.conf"
                  echo "Writing $file_vhost"
                  cat <<EOF > "$file_vhost"
                <IfDefine drupal>
                    <VirtualHost *:8443>
                      RDefaultUidGid apache apache
                      RUidGid     $user apache

                      ServerName  https://$site
                      ServerAdmin drupal-admins-$grou@cern.ch                      
                      $server_aliases

                      DocumentRoot "/mnt/data/drupal/$vers/"

                      <Directory "/mnt/data/drupal/$vers/">
                        AllowOverride None
                        Include "conf/drupal_htaccess.conf"
                      </Directory>

                      <Directory />
                        AuthType Shibboleth
                        ShibRequireSession $auth
                        ShibRequestSetting applicationId $site
                        ShibUseHeaders On
                        require shibboleth
                        require valid-user
                      </Directory>

                    </VirtualHost>
                </IfDefine>
                EOF

                  db_name=$(echo "$site_conf" | awk -F';' '{print $8}')
                  db_user=$(echo "$site_conf" | awk -F';' '{print $7}')
                  db_pass=$(echo "$site_conf" | awk -F';' '{print $9}')
                  db_host=$(echo "$site_conf" | awk -F';' '{print $6}' | awk -F':' '{print $1}')
                  db_port=$(echo "$site_conf" | awk -F';' '{print $6}' | awk -F':' '{print $2}')

                  file_settings="/mnt/data/drupal/${vers}/sites/${site}/settings.php"
                  echo "Writing $file_settings"
                  cat <<EOF > "$file_settings"
                <?php
                
                \$databases = array (
                  'default' =>
                  array (
                    'default' =>
                    array (
                      'database' => '$db_name',
                      'username' => '$db_user',
                      'password' => '$db_pass',
                      'host'     => '$db_host',
                      'port'     => '$db_port',
                      'driver' => 'mysql',
                      'prefix' => '',
                    ),
                  ),
                );

                \$update_free_access = FALSE;

                // ini_set should go to ini file ...
                ini_set('session.gc_probability', 1);
                ini_set('session.gc_divisor', 100);
                ini_set('session.gc_maxlifetime', 200000);
                ini_set('session.cookie_lifetime', 2000000);

                \$conf['memcache_servers'] = array();
                \$conf['lock_inc']='sites/all/modules/memcache/memcache-lock.inc';
                \$conf['cache_backends'][] = 'sites/all/modules/memcache/memcache.inc';
                // anonymous users - serve from cache directly
                \$conf['page_cache_invoke_hooks'] = FALSE;
                \$conf['page_cache_without_database'] = TRUE;

                \$conf['cache_default_class'] = 'MemCacheDrupal';
                \$conf['cache_class_cache_bootstrap'] = 'MemCacheDrupal';

                \$conf['cache_class_form_state'] = 'DrupalDatabaseCache';
                \$conf['cache_class_cache_form'] = 'DrupalDatabaseCache';
                \$conf['cache_class_cache_update'] = 'DrupalDatabaseCache';
                
                \$conf['memcache_servers'] = array(
                  'memc-0.memc.test-drupal-recovery-system.svc.cluster.local:11211' => 'default',
                  'memc-1.memc.test-drupal-recovery-system.svc.cluster.local:11211' => 'default',
                  'memc-2.memc.test-drupal-recovery-system.svc.cluster.local:11211' => 'default',
                );

                \$conf['memcache_bins'] = array(
                  'cache' => 'default',
                );
                
                \$conf['404_fast_paths_exclude'] = '/\/(?:styles)\//';
                \$conf['404_fast_paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe)$/i';
                \$conf['404_fast_html'] = "<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>\";

                \$conf['maintenance_theme'] = 'cern_maintenance';

                // Force https
                \$_SERVER['HTTPS'] = 'on';                

                \$conf['cernsearchexport_searchwsdl'] = 'http://searchpushwebservicesstag.web.cern.ch/searchpushwebservicesstag/drupaldev.asmx?WSDL';
                \$conf['cernsearch_resultspathnoframe'] = 'https://search.cern.ch/Pages/WebResults.aspx?k=';
                \$conf['cernsearch_resultspath'] = 'https://search.cern.ch/Pages/DrupalDevFrame.aspx?isFrame=1&httpsActivation=1&k=';
                EOF
                done
                echo "End: $(date)"

              image: openshift/base-centos7
              imagePullPolicy: Always
              name: rsync-official-sites
              resources:
                limits:
                  cpu: '1'
                  memory: 512Mi
                requests:
                  cpu: 100m
                  memory: 256Mi
              terminationMessagePath: /dev/termination-log
              terminationMessagePolicy: File
              volumeMounts:
              - mountPath: /mnt/data/drupal
                name: pvc-prod-drs
              - name: secretssh
                readOnly: true
                mountPath: /etc/secret-volume
              - mountPath: /etc/httpd/vhosts.d
                name: pvc-vhosts
              - mountPath: /etc/shibboleth/drupal
                name: pvc-shibboleth2
          dnsPolicy: ClusterFirst
          restartPolicy: Never
          serviceAccount: anyuid
          serviceAccountName: anyuid
          securityContext:
            runAsUser: 0
          terminationGracePeriodSeconds: 60
          volumes:
            - name: pvc-prod-drs
              persistentVolumeClaim:
                claimName: pvc-prod-drs
            - name: secretssh
              secret:
                defaultMode: 0400
                secretName: secretsshdrupaldev
            - name: pvc-vhosts
              persistentVolumeClaim:
                claimName: pvc-vhosts
            - name: pvc-shibboleth2
              persistentVolumeClaim:
                claimName: pvc-shibboleth2
