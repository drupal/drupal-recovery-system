# Deploy

```cat drupal-recovery-system.yml | oc create -f -```


# Requirements

 - There should be isolation between sites in place. Each site will run under a different user context as in production (it doesn't require to use the same usernames as production enviroment)
 - A prodecure must be created in order to apply the recovery system, this will include the deployment of the infrastructure and movement of the Official websites
 - Database and files should be retrieved from tape backups (in case that access to the NAS is not possible)
 - Databases should be instantiated on a special DBonDemand instance (Properly sized in order to perform as well as drupalp1)
 - Drush access is required in case we need to run things like Clear cache/registry rebuild while running on this recovery mode
 - Ideally Memcache container should be configured to reduce the load on the database
 - Not neccesary to replicate the additional parts of the infrastructure like drpadm, services, crons, webdav